let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101","Social Sciences 201"]
}

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
}

function introduce(student){

	console.log(`Hi! I'm ${student.name}. I am ${student.age} years old.`);
	console.log(`I study the following courses ${student.classes}`);
}

const getCube = (num) => num **3;

let cube = getCube(3);

console.log(cube);

let numArr = [15,16,32,21,21,2]

numArr.forEach(function(num){

	console.log(num);
})

let numsSquared = numArr.map(num => num ** 2)

console.log(numsSquared);

class Dog {
  constructor(name,breed,dogAge){
    this.name = name;
    this.breed = breed;
    this.dogAge = 7*dogAge;
  }
}

let dog1 = new Dog("Toby","Shih Tzu", 3);
let dog2 = new Dog("Ellie","Poodle", 4);

console.log(dog1);
console.log(dog2);
